var Observable = require('FuseJS/Observable');
var Glasses = require('Modules/Glasses');

var currentItem = Observable();

this.Parameter.onValueChanged(function(x) {
	// console.log('  the parameter changed to: ' + JSON.stringify(x));
	currentItem.value = x.id;
});

var info = currentItem.map(function(x) {
	return Glasses.list.getAt(x);
});

function goBack() {
	router.goBack();
};

module.exports = {
	'info': info,
	'goBack': goBack
};
