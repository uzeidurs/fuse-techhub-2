var Observable = require('FuseJS/Observable');
var Glasses = require('Modules/Glasses');

function exit() {
	router.goto('splash', {});
};

function openItem(args) {
	// console.log(' clicked: ' + JSON.stringify(args));
	router.push('productPage', {'id': args.data.id});
};

var largePic = Observable('');

function enlargePic(args) {
	console.log(args.data.image);
	largePic.value = args.data.image;
};

function collapsePic() {
	largePic.value = '';
};

var showLargePic = largePic.map(function(url) {
	if (url != '') {
		return true;
	} else {
		return false;
	}
});

module.exports = {
	'glasses': Glasses.list,
	'exit': exit,
	'openItem': openItem,
	'enlargePic': enlargePic,
	'collapsePic': collapsePic,
	'showLargePic': showLargePic,
	'largePic': largePic
};
