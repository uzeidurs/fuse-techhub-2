var Observable = require('FuseJS/Observable');

var tmp = [];
tmp.push(new Item(tmp.length, 'Stanford', 'g01.png', '€ 320', 'These eyeglasses are charismatically casual. This effortless style features a defined round shape with a classic keyhole cutout nose bridge and is finished in a semi-transparent muted shell hue. Clean lines and sharp colors make this look a stand out for both men and women.'));
tmp.push(new Item(tmp.length, 'Annabel', 'g02.png', '€ 240', 'These frames are another fabulous addition to our collection. This sleek design combines with fun color to bring a great daytime pair of glasses.'));
tmp.push(new Item(tmp.length, 'Panama', 'g03.jpg', '€ 530', 'Showcase your unique style with these gray floral eyeglasses. This feminine frame comes in a gray marbled patterned finish with rounded square shaped lenses. A keyhole nose bridge and double stud accents in the frame corners and temples add classic detailing to an updated look.'));
tmp.push(new Item(tmp.length, 'Dimension', 'g04.jpg', '€ 299', 'Adopt an air of sophistication with Dimension. This rimless frame is supported by gunmetal temples and anchored with an elegant nose bridge.'));
tmp.push(new Item(tmp.length, 'Brittany', 'g05.jpg', '€ 999', 'Get noticed in these brown floral eyeglasses. This full acetate frame comes in a uniquely semi-transparent brown floral patterned finish throughout and rounded square shaped lenses. Double stud accents are a finishing touch. Flexible spring hinges means you won?t have to sacrifice style for comfort.'));
tmp.push(new Item(tmp.length, 'New Bedford', 'g06.png', '€ 123', 'These La Femme eyeglasses are bold and flattering. With an outsize round shape and black acetate and metal design, this frame will instantly update your look.'));
tmp.push(new Item(tmp.length, 'Rhode Island', 'g07.jpg', '€ 777', 'Inject some powerful hipster style into your everyday look with these Juno matte navy eyeglasses. The bold, outsize round frames and chunky nose bridge contrast with sleek, slim line temples to create a frame that is trendy and flattering. Broad nose pads and matching temple tips mean Juno is also extra comfortable.'));
tmp.push(new Item(tmp.length, 'Mamba', 'g08.jpg', '€ 322', 'These golden brown eyeglasses are curvaceously luxurious. This rimless style features rectangular shaped lenses with gold detailing throughout. The artistic temples end in acetate arm tips. Adjustable nose pads create a subtly powerful look that is still comfortable.'));
tmp.push(new Item(tmp.length, 'Taylor', 'g09.jpg', '€ 748', 'These yellow eyeglasses are uniquely captivating and cool. The classic wayfarer style is given a makeover with the fusion of wood and plastic materials. The plastic frame front has a glossy striated wood like finish while the simulated wood temples are solid black. Flexible spring hinges offsets the durable rigidity.'));
tmp.push(new Item(tmp.length, 'Fiction', 'g10.jpg', '€ 183', 'These gray eyeglasses are lighthearted and sleek. This artful frame comes in a glossy beige acetate finish with oval shaped lenses. Classic elements like a keyhole nose bridge and flexible metallic gold temples create a look that is adaptable for both men and women.'));
tmp.push(new Item(tmp.length, 'Sweet Janet', 'g11.jpg', '€ 320', 'Make a bold statement with these clear eyeglasses. The classic wayfarer style is updated in a transparent opaque plastic. A keyhole nose bridge adds a trendy detail. For added comfort, the arms are attached with spring hinges.'));
tmp.push(new Item(tmp.length, 'Enzo', 'g12.jpg', '€ 344', 'These navy and silver eyeglasses are subtly slinky. This metal frame features an emphasized browline in a matte navy metallic finish that extends to the temples. Silver detailing border oval shaped lenses. Adjustable nose pads, flexible spring hinges and acetate arm tips make this willowy style comfortable for both men and women.'));
tmp.push(new Item(tmp.length, 'Rita', 'g13.jpg', '€ 543', 'Add a little extra to the ordinary with these tortoise eyeglasses. This trendy look features round lenses and a vintage inspired keyhole bridge. The elegant tortoiseshell acetate is bold yet versatile, creating an undeniably geek-chic look. Double stud accents complete this New York inspired look.'));
tmp.push(new Item(tmp.length, 'Vendome', 'g14.jpg', '€ 33', 'These brown and tortoise eyeglasses are simple yet radiant. This full plastic frame comes in a glossy brown tortoiseshell finish with subtle cat eye horn shaped lenses that add a touch of character. The temples are slender and complement this feminine yet strong look.'));
tmp.push(new Item(tmp.length, 'La Femme', 'g15.jpg', '€ 112', 'These tortoise eyeglasses are captivatingly unexpected. This full acetate frame comes in a fiery tortoiseshell finish throughout with subtly horned wayfarer shaped lenses. Double stud accents add subtle detailing. Flexible spring hinges make this iconic look universally flattering and comfortable.'));

var list = Observable();
list.replaceAll(tmp);

function Item(id, name, image, price, description) {
	this.id = id;
	this.name = name;
	this.image = 'https://dl.dropboxusercontent.com/u/11750787/fuse-techhub-2/' + image;
	this.price = price;
	this.description = description;
};

module.exports = {
	'list': list
};
